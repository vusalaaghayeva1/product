package az.ingress.product.dto;

import az.ingress.product.enums.Category;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProductCountAndByCategory {
    Category category;
    Long count;
}
