package az.ingress.product.controller;

import az.ingress.product.criteria.SearchCriteria;
import az.ingress.product.dto.ProductCountAndByCategory;
import az.ingress.product.dto.ProductRequest;
import az.ingress.product.dto.ProductResponse;
import az.ingress.product.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;

    @PostMapping
    public ResponseEntity<ProductResponse> create(@RequestBody ProductRequest dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(productService.createProduct(dto));
    }

    @GetMapping("/list")
    public ResponseEntity<List<ProductResponse>> getAll(Integer priceFrom, Integer priceTo) {
        return ResponseEntity.ok(productService.getAllProducts(priceFrom, priceTo));
    }

    @GetMapping("/count-by-category")
    public ResponseEntity<List<ProductCountAndByCategory>> getCountByCategory() {
        return ResponseEntity.ok(productService.getCountByCategory());
    }

    @GetMapping("/criteria")
    public ResponseEntity<Page<ProductResponse>> getProductByCriteria(@RequestBody List<SearchCriteria> criteria, Pageable pageable) {
        return ResponseEntity.ok(productService.getProductByCriteria(criteria, pageable));
    }
}

