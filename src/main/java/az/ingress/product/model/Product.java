package az.ingress.product.model;
import az.ingress.product.enums.Category;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "products")
@FieldDefaults(level=AccessLevel.PRIVATE)
@Builder
public class Product {
@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
@Column(unique = true)
    String name;
    Integer price;
    @Column(columnDefinition = "TEXT")
    String description;
    @Enumerated(EnumType.STRING)
    Category category;
}
