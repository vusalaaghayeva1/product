package az.ingress.product.service;

import az.ingress.product.criteria.ProductSpecification;
import az.ingress.product.criteria.SearchCriteria;
import az.ingress.product.dto.ProductCountAndByCategory;
import az.ingress.product.dto.ProductRequest;
import az.ingress.product.dto.ProductResponse;
import az.ingress.product.model.Product;
import az.ingress.product.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static az.ingress.product.mapper.ProductMapper.PRODUCT_MAPPER;


@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    @Override
    public ProductResponse createProduct(ProductRequest productRequest) {
       Product product= PRODUCT_MAPPER.mapRequestToEntity(productRequest);
       productRepository.save(product);
       return PRODUCT_MAPPER.mapEntityToResponse(product);

    }

    @Override
    public List<ProductResponse> getAllProducts(Integer priceFrom, Integer priceTo) {
        List<Product> products;

        if (priceFrom != null && priceTo != null) {
            products = productRepository.customFindAllByPriceGreaterThanEqualAndPriceLessThanEqual(priceFrom, priceTo);
        } else if (priceFrom == null && priceTo != null) {
            products = productRepository.customFindAllByPriceLessThan(priceTo);
        } else if (priceFrom != null) { // priceTo == null
            products = productRepository.customFindAllByPriceGreaterThan(priceFrom);
        } else {
            products = productRepository.findAll();
        }

        return products.stream()
                .map(PRODUCT_MAPPER::mapEntityToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public List<ProductCountAndByCategory> getCountByCategory() {
        return productRepository.customFindByCategory();
    }


    @Override
    public Page<ProductResponse> getProductByCriteria(List<SearchCriteria> criteria, Pageable pageable) {
        ProductSpecification specification = new ProductSpecification();
        criteria.forEach(specification::add);
        return productRepository.findAll(specification, pageable).map(PRODUCT_MAPPER::mapEntityToResponse);
    }

}




