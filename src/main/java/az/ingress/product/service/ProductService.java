package az.ingress.product.service;

import az.ingress.product.criteria.SearchCriteria;
import az.ingress.product.dto.ProductCountAndByCategory;
import az.ingress.product.dto.ProductRequest;
import az.ingress.product.dto.ProductResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProductService {
    ProductResponse createProduct(ProductRequest dto);

    List<ProductResponse> getAllProducts(Integer priceFrom, Integer priceTo);

    List<ProductCountAndByCategory> getCountByCategory();

    Page<ProductResponse> getProductByCriteria(List<SearchCriteria> criteria, Pageable pageable);


}