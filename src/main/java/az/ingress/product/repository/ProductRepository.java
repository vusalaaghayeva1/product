package az.ingress.product.repository;

import az.ingress.product.dto.ProductCountAndByCategory;
import az.ingress.product.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> , JpaSpecificationExecutor<Product> {

    boolean existsByName(String name);

    @Query("SELECT p FROM Product p WHERE p.price >= ?1 AND p.price <= ?2")
    List<Product> customFindAllByPriceGreaterThanEqualAndPriceLessThanEqual
            (Integer priceFrom, Integer priceTo);

    @Query(value = "SELECT p from Product p where p.price>= ?1")
    List<Product> customFindAllByPriceGreaterThan(Integer priceFrom);

    @Query(value = "SELECT p from Product p where p.price<= ?1")
    List<Product> customFindAllByPriceLessThan(Integer priceTo);

    @Query(value = "SELECT new az.ingress.product.dto.ProductCountAndByCategory(s.category, COUNT(s)) FROM Product s GROUP BY s.category")
    List<ProductCountAndByCategory> customFindByCategory();


}
