package az.ingress.product.mapper;

import az.ingress.product.dto.ProductRequest;
import az.ingress.product.dto.ProductResponse;
import az.ingress.product.model.Product;

public enum ProductMapper {
    PRODUCT_MAPPER;           //manual mapper was used in here

    public Product mapRequestToEntity(ProductRequest request){
       return Product.builder()
                .name(request.getName())
                .description(request.getDescription())
                .price(request.getPrice())
                .category(request.getCategory())
                .build();

    }

    public ProductResponse mapEntityToResponse(Product product){

       return ProductResponse.builder()
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getPrice())
                .category(product.getCategory())
                .build();
    }

}
