# Use official PostgreSQL image from Docker Hub
FROM postgres:latest

# Environment variables
ENV POSTGRES_DB=mydatabase
ENV POSTGRES_USER=myuser
ENV POSTGRES_PASSWORD=mypassword

# Expose the PostgreSQL port
EXPOSE 5435

# Optionally, set the working directory if needed
# WORKDIR /path/to/working/directory

# Optionally, copy initialization scripts or custom configurations
# COPY init.sql /docker-entrypoint-initdb.d/

# Command to run PostgreSQL
CMD ["postgres"]

# Example of CMD if you want to run PostgreSQL as a foreground process:
# CMD ["postgres", "-c", "config_file=/etc/postgresql/postgresql.conf"]